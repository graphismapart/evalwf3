<?php

// TABLEAU Liste

$user_array = array(
        "Nom"=> "Maud",
        "Prénom"=> "Poulain",
        "Adresse"=>"27, rue bouguereau",
        "Code Postal"=> 59910,
        "Ville"=> "Lille",
        "Email"=> "poulainmaud@gmail.com",
        "Téléphone"=> "06 72 32 01 59",
        "Date de naissance"=> "1986-05-31"
      );


?>

<!-- Code HTML -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title> On se présente !</title>

  </head>
  <body>

    <h1> On se présente !</h1>

    <div>
      <?php foreach ($user_array  as $user => $value ): ?>
         <ul>
           <li>
               <?php if( $user == "Date de naissance") {
                     $date = new DateTime($value);
                     echo $user." : ". $date->format('d/m/Y');
                 } else {
                     echo $user." : ". $value;
                 }
                ?>
            </li>
         </ul>
      <?php endforeach; ?>
  </div>

  </body>
</html>
