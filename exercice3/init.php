<?php
include_once 'config.php';



// --------------------
// INITIALISATION DE SESSION
// --------------------

session_start();


// --------------------
// CONNEXION BDD
// --------------------

// Création de la connexion à la base de données
$pdo = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $user, $pass);
