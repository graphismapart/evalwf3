<?php
include_once 'config.php';
include_once 'init.php';


/**
 * Get Token
 * Generate and return a token string
 * @return (string) Token
 */
function getToken() {
    return md5(uniqid());
}




// On defini les variables par defaut
$title          = null;
$actors         = null;
$director       = null;
$producer       = null;
$year_of_prod   = null;
$language       = null;
$category       = null;
$storyline      = null;
$video          = null;


// On controle si l'utilisateur envoi(e) le formulaire remplie
if (isset($_POST['sendRegistration'])) {
    // Récuperation des données du formulaire (et verification si tous les champs sont présents à l'envoi)
    // Récupération des données du formulaire
    $title          = isset($_POST['title']) ? trim ($_POST['title']) : null;
    $actors         = isset($_POST['actors']) ? $_POST['actors'] : null;
    $director       = isset($_POST['director']) ? trim ($_POST['director']) : null;
    $producer       = isset($_POST['producer']) ? trim ($_POST['producer']) : null;
    $year_of_prod   = isset($_POST['year']['of']['prod']) ? trim ($_POST['year']['of']['prod']) : null;
    $language       = isset($_POST['language']) ? trim ($_POST['language']) : null;
    $category       = isset($_POST['category']) ?trim ( $_POST['category']) : null;
    $storyline      = isset($_POST['storyline']) ? trim ($_POST['storyline']) : null;
    $video          = isset($_POST['video']) ? trim ($_POST['video']) : null;



  // Peut on enregistrer les donnes du formulaire dans le BDD?
  // Par defaut : Oui
  // On surchargera avec la valeur false dans le cas ou le controle du formulaire
  // detecte une erreur.
  $send = true;

  // Verifie l'integralité du token
  if ($_SESSION['token'] !== $_POST['token']) {
      $send = false;
      echo '"<div class="alert alert-warning" role="alert">Le token est invalide.</div>"';
  }

  // Controle des données envoyées par l'utilisateur

  // Controle Les champs “titre, nom du réalisateur, acteurs, producteur et synopsis” comporteront
  // - au minimum 5 caractères.

    $controle = strlen($title, $actors, $director, $producer, $storyline );

    if ($controle < 5 ) {
        $send = false;
        echo '"<div class="alert alert-warning" role="alert">Attention ! Le champ doit contenir au minimum 5 caractères.</div>"';
    }


// // ● Les champs : année de production, langue, category, seront obligatoirement un
//      menu déroulant
// ● Le lien de la bande annonce sera obligatoirement une URL valide

    if (filter_var($video, FILTER_VALIDATE_URL)) {
        $send = false;
        echo '"<div class="alert alert-warning" role="alert">$video is not a valid URL</div>")';
    }

// ● En cas d’erreurs de saisie, des messages d’erreurs seront affichés en rouge





  // Controle des conditions d'utilisation du service

  // Controle l'existance de l'utilisateur dans la BDD
  // L'adresse mail ne doit pas etre presente dans la BDD

  if ($send) {
    // Enregistrement du Film et ces paramètres
    $q = "INSERT INTO `movies`(`title`, `actors`, `director`, `producer`, `year_of_prod`, `language`, `category`, `storyline`, `video`)
                    VALUES (:title,  :actors,  :director,  :year_of_prod , :language, :category, :storyline, :video, NOW())";
    $q = $pdo->prepare($q);
    $q->bindValue(":title", $title, PDO::PARAM_STR);
    $q->bindValue(":actors", $actors, PDO::PARAM_STR);
    $q->bindValue(":director", $director, PDO::PARAM_STR);
    $q->bindValue(":producer", $producer, PDO::PARAM_STR);
    $q->bindValue(":year_of_prod", $year_of_prod, PDO::PARAM_STR);
    $q->bindValue(":language", $language, PDO::PARAM_STR);
    $q->bindValue(":category", $category, PDO::PARAM_STR);
    $q->bindValue(":storyline", $storyline, PDO::PARAM_STR);
    $q->bindValue(":video", $video, PDO::PARAM_STR);

    $q->execute();
    $q->closeCursor();

    // Identification de l'utilisateur

    // Redirection vers sa page de profil

  }

}



else {
  // On génére le token
  // Le token servira a verifier l'integralité du formulaire
  $_SESSION['token'] = md5(uniqid());

}

 ?>


<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  </head>
  <body class="container">



    <h1>Ajoutez un Film !</h1>

    <form class="" action="formulaire.php" method="post">

      <!-- Les champs “titre, nom du réalisateur, acteurs, producteur et synopsis” comporteront
au minimum 5 caractères.
  -->
              <br>
          <input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>">
          <div>
              <label for="title">Le nom du film</label>
              <input type="text" id="title" name="title" value="<?php echo $title; ?>">
          </div>
              <br>
          <div>
              <label for="actors">Les noms d’acteurs</label>
              <input type="text" id="actors" name="actors" value="<?php echo $actors; ?>">
          </div>
            <br>
          <div>
              <label for="director">Le nom du réalisateur</label>
              <input type="text" id="director" name="director" value="<?php echo $director; ?>">
          </div>
            <br>
          <div>
              <label for="producer">Le nom du producteur</label>
              <input type="text" id="producer" name="producer" value="<?php echo $producer; ?>">
          </div>
              <br>
          <div>
              <label for="storyline">le synopsis du film</label>
              <input type="text" id="storyline" name="storyline" value="<?php echo $storyline; ?>">
          </div>

          <!--  Les champs : année de production, langue, category, seront obligatoirement un menu déroulant -->

          <br>

        <div>
            <label for="year_of_prod">l’année de production</label>
            <select id="year_of_prod" name="year_of_prod" value="<?php echo $year_of_prod; ?>">
              <option value="">L’année de production</option>
              <?php for($i= date('Y'); $i>=date('Y')-100; $i--): ?>
                <option value="<?php echo $i; ?>"><?php
                 echo $i;
                 ?></option>
                <?php endfor; ?>
            </select>
        </div>
        <br>
        <div>
          <label for="language">la langue du film</label>
            <select name="language" value="<?php echo $language; ?>">
              <option value="">la langue du film</option>
              <?php for($i=1; $i<2; $i++): ?>
              <option value="<?php echo ($i+1); ?>"><?php
               echo $month[$i];
               ?></option>
              <?php endfor; ?>
            </select>
        </div>
        <br>
        <div>
            <label for="category">la catégorie du film</label>
            <select name="category" value="<?php echo $category; ?>">
              <option value="">la catégorie du film</option>
              <?php for($i=1 ; $i>=3 ; $i++): ?>
              <option value="<?php echo $i; ?>"><?php
               echo $i;
               ?></option>
              <?php endfor; ?>
            </select>
        </div>
        <br>
        <button type="submit" name="sendRegistration">Enregistrer Le Film</button>
    </form>




    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </body>
</html>
