<?php
$euro = null;

if (isset($_POST['validate'])) {

    // Contrôle des paramètres--------------------------
    $send = true;

    $euro = isset($_POST['euro']) ? trim($_POST['euro']) : null;

    if (empty($euro)) {
      $send = false;
      echo "Attention " , " Indiquer une valeur.";

    }
    elseif (!empty($euro)) {
      $send = true;
    }

    if (!preg_match('#[^a-zA-Z-]#', $euro)) {
          $send = true;
          echo "Attention " , " Indiquer une valeur numérique.";
        }
  



    // Fonction de conversion des euros en dollars américains.
    function convert($euros,$exchange)
    {
        $euros = $euros*$exchange;
        return $euros;
    }

    $convert = convert($_POST['euro'],1.1457);
}

 ?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <title>On part en voyage</title>
    <meta name="description" content="">
  </head>
    <body>


        <!-- Formulaire du convertisseur -->
        <h3>convertisseur des euros en dollars</h3>

        <form method="POST" name="convertEurUsd">
            <input type="text" name="euro" id="valeur" placeholder="Entrez une valeur en euros">
            <input type="submit" name="validate" value="Convertir">
        </form>
          <br>

        <!-- Affichage du resultat de la conversion de l'euros en dollars -->
        <?php
        global $convert;
        if (isset($convert)){
        echo $euro." euros = ".$convert." dollars américains";
        }
        ?>

    </body>
</html>
